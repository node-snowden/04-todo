const fs = require('fs');

const file = './db/data.json';

const saveDB = (data) => {

	fs.writeFileSync(file, JSON.stringify(data));
}

const readDB = (data) => {
	if (!fs.existsSync(file)) {
		return null;
	}

	const info = fs.readFileSync(file, {encoding: 'utf-8'});
	const fileData = JSON.parse(info);

	return fileData;
}

module.exports = {
	saveDB,
	readDB
}

