require('colors');
const {saveDB, readDB} = require('./helpers/saveFile');
const {showChecklist, inquirerMenu, pause, readInput, listTaskDelete, confirm} = require('./helpers/inquirer');
const Tareas = require('./models/tareas');

console.clear();

const main = async () => {

	let opt = '';
	const tareas = new Tareas();

	const tareaDB = readDB();

	if (tareaDB) {
		tareas.cargarTareasFromArray(tareaDB);
	}

	do {

		// Imprimir el menú
		opt = await inquirerMenu();

		switch (opt) {

			case '1':
				const desc = await readInput('Descripción:');
				tareas.crearTarea(desc);
				break;
			case '2':
				tareas.listadoCompleto()
				break;
			case '3':
				tareas.listarPendientesCompletadas(true);
				break;
			case '4':
				tareas.listarPendientesCompletadas(false);
				break;
			case '5':
				const ids = await showChecklist(tareas.listadoArr); showChecklist
				tareas.toogleCompleted(ids);
				break;
			case '6':
				const id = await listTaskDelete(tareas.listadoArr);

				if (id !== 0) {

					const ok = await confirm('¿Estas seguro?')
					if (ok) {
						tareas.borrarTarea(id);
						console.log('Tarea borrada');
					}
				}
				break;

		}
		// console.log(opt);
		saveDB(tareas.listadoArr)
		await pause();
	} while (opt !== '0');
}

main();
