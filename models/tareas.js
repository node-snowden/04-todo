require('colors');
const Tarea = require('./tarea');
/***s
 * _listado: 
 * 			{'uuid-123123-123123': { id: 12, desc: asd, completeIn: 123 }}
 */

class Tareas {

	_listado = {};

	get listadoArr() {
		const listado = [];
		Object.keys(this._listado).forEach(key => {
			const tarea = this._listado[key];
			listado.push(tarea);
		});
		return listado;
	}

	constructor() {
		this._listado = {};
	}

	borrarTarea(id = '') {
		if (this._listado[id]) {
			delete this._listado[id];
		}
	}

	cargarTareasFromArray(tareas = []) {
		tareas.forEach(tarea => {
			this._listado[tarea.id] = tarea;
		});
	}

	crearTarea(desc = '') {
		const tarea = new Tarea(desc);
		this._listado[tarea.id] = tarea;
	}

	listadoCompleto() {

		console.log();
		this.listadoArr.forEach((tarea, i) => {
			const idx = `${i + 1}`.green;
			const {desc, completedIn} = tarea;
			const estado = completedIn ? ' Completada'.green : 'Pendiente'.red;
			console.log(`${idx} ${desc} :: ${estado}`);
		})

	}

	listarPendientesCompletadas(completadas = true) {

		console.log();
		let contador = 0;
		this.listadoArr.forEach((tarea) => {
			const {desc, completedIn} = tarea;
			const estado = completedIn ? ' Completada'.green : 'Pendiente'.red;

			if (completadas) {
				if (completedIn) {
					contador += 1;
					console.log(`${(contador + '.').green} ${desc} :: ${completedIn.green}`);
				}
			} else {
				if (!completedIn) {
					contador += 1;
					console.log(`${(contador + '.').green} ${desc} :: ${estado}`);
				}
			}
		})
	}


	toogleCompleted(ids = []) {
		ids.forEach((id) => {
			const tarea = this._listado[id];

			if (!tarea.completedIn) {
				tarea.completedIn = new Date().toISOString();
			}
		});

		this.listadoArr.forEach(tarea => {
			if (!ids.includes(tarea.id)) {
				this._listado[tarea.id].completedIn = null;
			}

		});
	}
}
module.exports = Tareas;
